package com.todo.erp.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.todo.erp.service.services.ClienteService;

@Controller
public class MainController {
	
	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping("/home")
	 public String paginaInicial(Model model) {
	  model.addAttribute("clientes", clienteService.findAll());
	  return "index";
	 }
	
}
