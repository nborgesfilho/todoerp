package com.todo.erp.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todo.erp.domain.model.Cliente;


public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	public List<Cliente> findAll();
}
