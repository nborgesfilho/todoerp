package com.todo.erp.service.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todo.erp.domain.model.Cliente;
import com.todo.erp.domain.repository.ClienteRepository;

@Service("ClienteService")
public class ClienteService {

	public ClienteService(){
		
	}
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	public List<Cliente> findAll(){
		return clienteRepository.findAll();
	}
}
